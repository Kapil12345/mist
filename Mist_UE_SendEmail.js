/*******************************************************************
*
*
* Name: Mist_UE_SendEmail.js
* Script Type: User Event
* Version: 1.0
*
*
* Author: Kapil(Lirik Inc.)
* Purpose: This script is used to send email to sales rep and/
* Script: The script record id
* Deploy: The script deployment record id
*
*
* ******************************************************************* */

function sendEmailToOpTeamAfterSubmit(type){
  try {
    var salesRep = nlapiGetFieldValue('salesrep'), emailSalesRep;
    nlapiLogExecution('DEBUG','salesRep',salesRep);
    if(salesRep)
      emailSalesRep = nlapiLookupField('employee',salesRep,'email');
    if(type == 'create' && emailSalesRep){
      var subject = 'Sales Order has been created';
      var body = 'Hi,<br/> A sales order has been created with id '+nlapiGetRecordId();
      //nlapiSendEmail(nlapiGetUser(),salesRep,subject,body);
    }
    else if(type == 'edit' ){
    var shipDate = nlapiGetFieldValue('shipdate');
    var oldRecObj = nlapiGetOldRecord();
    var prevShipDate = oldRecObj.getFieldValue('shipdate');
    nlapiLogExecution('DEBUG','shipDate & prevShipDate',shipDate+' & '+prevShipDate);
    if(shipDate != prevShipDate && emailSalesRep){
      var subject = '';
      //nlapiSendEmail(author,recipient,subject,body,cc,bcc,records,attachments,notifySenderOnBounce,internalOnly,replyTo)
    }
    }
  } catch (err) {
    nlapiLogExecution('ERROR','Error sendEmailToOpTeamAfterSubmit',err);
  }
}
