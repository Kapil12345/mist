/*******************************************************************
 *
 *
 * Name: Mist_Update_Revenue_Arrangement.js
 * Script Type: User Event
 * Version: 1.0
 *
 *
 * Author: Kapil
 * Purpose: This script is used to update revenue arrangement
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function updateRevArrangementsBeforeSubmit(type) {
  try {
    var dateVal = nlapiGetFieldValue('trandate');
    if (dateVal) {
      var revLineCount = nlapiGetLineItemCount('revenueelement');
      nlapiLogExecution('DEBUG','updateRevArrangementsBeforeSubmit','revLineCount::'+revLineCount);
      if (revLineCount > 0) {
        for (var revIndex = 1; revIndex <= revLineCount; revIndex++) {
          nlapiSetLineItemValue('revenueelement', 'forecaststartdate', revIndex, dateVal);
          //nlapiSetCurrentLineItemValue('revenueelement','forecaststartdate',dateVal,firefieldchanged,synchronous)
        }
      }
    }
  } catch (err) {
    nlapiLogExecution('DEBUG', 'Error in updateRevArrangementsBeforeSubmit()::', err);
  }
}
