/*******************************************************************
*
*
* Name: Mist_SC_Hold_RevRecPlans.js
* Script Type: Scheduled
* Version: 1.0
*
*
* Author: Kapil(Lirik Inc.)
* Purpose: This script is used on revenue recognition, revenue element and item fulfillment for their updation.
* Script: The script record id
* Deploy: The script deployment record id
*
*
* ******************************************************************* */

function updateRevenueRec(type){
  try {
    var revRecFilters = [], revRecColumns = [];
    revRecFilters.push(new nlobjSearchFilter('holdrevenuerecognition',null,'is','F'), new nlobjSearchFilter('revenueplantype',null,'is','ACTUAL'),new nlobjSeachFilter('custrecord_pod_updated',null,'is','F'),new nlobjSearchFilter('datecreated',null,'onorafter','yesterday'));
    revRecColumns.push(new nlobjSearchColumn('holdrevenuerecognition',null,'GROUP'),new nlobjSearchColumn('internalid',null,'GROUP'),new nlobjSearchColumn('createdfrom',null,'GROUP'));
    var searchRevRecPlans = nlapiSearchRecord('revenueplan',null,revRecFilters,revRecColumns);
    if(searchRevRecPlans){
      nlapiLogExecution('DEBUG','searchRevRecPlans,-- searchLength',searchRevRecPlans+'&&&'+searchRevRecPlans.length);
      for(var searchindex=0; searchIndex<searchRevRecPlans.length; searchIndex++){
        var revRecId = searchRevRecPlans[i].getValue('internalid',null,'GROUP');
        var createdFrom = searchRevRecPlans[i].getValue('createdfrom',null,'GROUP');
        var getItemFulfillmentInfo = searchRevenueElement(createdFrom,revRecId);
      }
    }
  } catch (err) {
    nlapiLogExecution('ERROR','Error in scheduled script',err);
  }
}

//Function to search Customer on Revenue Element
function searchRevenueElement(createdFrom,revenuePlanId) {
  try {
    if (createdFrom) {
      var revElementFilters = [],
        revElementColumns = [];
      revElementFilters.push(new nlobjSearchFilter('internalid', null, 'is', createdFrom));
      revElementColumns.push(new nlobjSearchColumn('entity'));
      var searchRevElementRec = nlapiSearchRecord('revenueelement', null, revElementFilters, revElementColumns);
      if (searchRevElementRec) {
        var revElementCustomer = searchRevElementRec[0].getValue('entity');
        if (revElementCustomer) {
          var searchPODCustomer = getPODCustomer(revElementCustomer);
          if(searchPODCustomer){
            nlapiSubmitField('revenueplan',revenuePlanId,'holdrevenuerecognition','T');
          }
        }
      }
    }
  } catch (err) {
    nlapiLogExecution('ERROR','Error in searchRevenueElement::',err);
  }
}

//Function to search Customer on ccustom record "POD Customers" matched with customer on Revenue Element
function getPODCustomer(revElementCustomer) {
  try {
    nlapiLogExecution('DEBUG', 'updateRevenueRec', 'revElementCustomer::' + revElementCustomer);
    if (revElementCustomer) {
      var PODFilters = [], PODColumns = [];
      PODFilters.push(new nlobjSearchFilter('custrecord_pod_customers',null,'is',revElementCustomer),new nlobjSearchFilter('isinactive',null,'is','F'));
      var searchPODCustomer = nlapiSearchRecord('customrecord_pod_customers',null,PODFilters,null);
      return searchPODCustomer;
    }
  } catch (err) {
nlapiLogExecution('ERROR','Error in getPODCustomer::',err);
  }
}
