/*******************************************************************
 *
 *
 * Name: Mist_SS_upload_serial_Nos_file.js
 * Script Type: Suitelet
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function updateSerialNumbers(request, response) {
  try {
    var context = nlapiGetContext();
    var loggedInUserEmail = context.getEmail();
    //var vendorRec = searchVendor(loggedInUserEmail);
    var recordId = request.getParameter('custparam_record_id');
    var recordType = request.getParameter('custparam_record_type');

    if (request.getMethod() == 'GET') {
      var serialForm = nlapiCreateForm('Upload Serial Numbers');
      var fileFld = serialForm.addField('custpage_serialnos_uploadfile', 'file', 'Select File');
      fileFld.setMandatory(true);
      serialForm.addField('custpage_recordtype', 'text', '', null).setDisplayType('hidden').setDefaultValue(recordType);
      serialForm.addField('custpage_record_id', 'text', '', null).setDisplayType('hidden').setDefaultValue(recordId);
      serialForm.addSubmitButton('Submit');
      response.writePage(serialForm);
    } else {
      var file = request.getFile('custpage_serialnos_uploadfile'),
        fileId;

      if (file) {
        var folderFil = [];
        folderFil.push(new nlobjSearchFilter('name', null, 'is', 'Serial Number Upload'));

        var folderRslt = nlapiSearchRecord('folder', null, folderFil, null);
        if (folderRslt)
          file.setFolder(folderRslt[0].getId());

        var recordTypePOST = request.getParameter('custpage_recordtype');
        var recordIdPOST = request.getParameter('custpage_record_id');
        // Create file and upload it to the file cabinet.
        fileId = nlapiSubmitFile(file);
        nlapiLogExecution('DEBUG', 'File id submitted', fileId);
        if (fileId) {

          var strScriptID = 'customscript_mist_sc_generate_serialnos';
          var strScriptDeploymentID = 'customdeploy_mist_sc_generate_serialnos';

          nlapiLogExecution('DEBUG', 'parseCSVFile', 'fileId::' + fileId + 'recType::' + recordTypePOST + 'recId::' + recordIdPOST);
          var scriptParameters = new Array();
          scriptParameters['custscript_record_type'] = recordTypePOST;
          scriptParameters['custscript_record_id'] = recordIdPOST;
          scriptParameters['custscript_fileid'] = fileId;
          var scheduleScriptInstance = nlapiScheduleScript(strScriptID, strScriptDeploymentID, scriptParameters);
        }
      }
      response.write('<html><body><script>window.close();window.opener.location.reload();</script></body></html>');
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error in Suitelet', e);
  }
}
