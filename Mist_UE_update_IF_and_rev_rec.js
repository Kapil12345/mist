/*******************************************************************
*
*
* Name: Mist_UE_update_IF_and_rev_rec.js
* Script Type: User event
* Version: 1.o
*
*
* Author: Kapil(Lirik Inc.)
* Purpose: This script is used for customization IF & Rev Rec when item fulfillment is updated.
* Script: The script record id
* Deploy: The script deployment record id
*
*
* ******************************************************************* */
//After Submit Event on item fulfillment for POD and hold rev rec plans
function updateRevenueRecognitionPlan(type){
 try
 {
   if(type == 'create' || type == 'edit')
   {
     var newPOD = nlapiGetFieldValue('custbody_proof_of_delivery');
     var oldFulfillmentObj = nlapiGetOldRecord();
     var oldFulfillmentPOD = oldFulfillmentObj.getFieldValue('custbody_proof_of_delivery');
     var tranid = nlapiGetFieldValue('tranid');
     if((newPOD) && (newPOD != oldFulfillmentPOD))
     {
       nlapiSubmitField('itemfulfillment',nlapiGetRecordId(),'trandate',newPOD);
     }
     var revRecFilters = [], revRecColumns = [];
     revRecFilters.push(new nlobjSearchFilter('creationtriggeredby',null,'is','Item Fulfillment #'+tranid));
     revRecFilters.push(new nlobjSearchFilter('revenueplantype',null,'is','ACTUAL'));
     revRecColumns.push(new nlobjSearchColumn('holdrevenuerecognition'));

     var search_result = nlapiSearchRecord('revenueplan',null,revRecFilters,revRecColumns);
     if (search_result)
     {
        for(var i=0; i<search_result.length; i++)
        {
           //var holdrevenuerecognition = nlapiLookupField('revenueplan',search_result[0].getId(),'holdrevenuerecognition');
           var holdrevenuerecognition = search_result[0].getValue('holdrevenuerecognition');
           if(holdrevenuerecognition == "T")
           {
             var id = nlapiSubmitField('revenueplan',search_result[0].getId(),'holdrevenuerecognition','F');
             nlapiSubmitField('revenueplan',search_result[0].getId(),'custrecord_pod_updated','T');
           }
        }
     }
  }
 }
 catch(err)
 {
   nlapiLogExecution('ERROR','Error in updateRevenueRecognitionPlan::',err);
 }
}
