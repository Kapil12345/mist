/*******************************************************************
 *
 *
 * Name: Mist_SC_Update_Serial_Nos.js
 * Script Type: Scheduled Script
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used to update the serial nos.
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function updateSerialNumbers(type) {
  try {
    var itemDetailFilters = [],
      itemDetailColumns = [];
    itemDetailFilters.push(new nlobjSearchFilter('custrecord_record_status', null, 'anyof', [1, 3]), new nlobjSearchFilter('isinactive', null, 'is', 'F'));
    itemDetailColumns.push(new nlobjSearchColumn('custrecord_item_name'), new nlobjSearchColumn('custrecord_serial_number'),
      new nlobjSearchColumn('custrecord_carton_id'), new nlobjSearchColumn('custrecord_claim_code'), new nlobjSearchColumn('custrecord_mac_id'), new nlobjSearchColumn('custrecord_pallet_id'), new nlobjSearchColumn('custrecord_invoice_no'));

    //Search created to get the custom record 'Inventory Detail' entries

    var searchItemDetailRecords = nlapiCreateSearch('customrecord_inventory_detail_record', itemDetailFilters, itemDetailColumns);
    var searchResults = searchItemDetailRecords.runSearch();
    var resultIndex = 0,
      resultStep = 1000,
      resultSet;
    do {
      resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
      resultIndex = parseInt(resultIndex + resultStep);
      for (var searchIndex = 0; searchIndex < resultSet.length; searchIndex++) {
        var serialNoObj = {};
        serialNoObj.serialNo = resultSet[searchIndex].getValue('custrecord_serial_number');
        serialNoObj.itemId = resultSet[searchIndex].getValue('custrecord_item_name');
        serialNoObj.macId = resultSet[searchIndex].getValue('custrecord_mac_id');
        serialNoObj.palletId = resultSet[searchIndex].getValue('custrecord_pallet_id');
        serialNoObj.invoiceNo = resultSet[searchIndex].getValue('custrecord_invoice_no');
        if(serialNoObj.serialNo && serialNoObj.itemId)
          searchSerialNumberRecordsNS(serialNoObj);
      }
    } while (resultSet.length > 0);
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error caught in updateSerialNumbers()::', e);
  }
}

function searchSerialNumberRecordsNS(serialNoObj){
  try {

  } catch (e) {
  nlapiLogExecution('ERROR','Error caught in searchSerialNumberRecordsNS()::',e);
  }
}
