/*******************************************************************
 *
 *
 * Name: Mist_UE_Set_Purchase_Price.js
 * Script Type: User Event Script
 * Version: 1.1
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used to set the item's purchase price on item receipt
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function setPurchasePriceBeforeSubmit(type) {
  if (type == 'create' || type == 'edit') {
    var createdFrom = nlapiGetFieldValue('createdfrom');
    var getRecordType = searchRecordType(createdFrom);
    if (getRecordType == 'returnauthorization') {
      var itemCount = nlapiGetLineItemCount('item');
      if (itemCount > 0) {
        for (var itemIndex = 1; itemIndex <= itemCount; itemIndex++) {
          var itemId = nlapiGetLineItemValue('item', 'item', itemIndex);
          var itemPOPriceSetOnUI = nlapiGetLineItemValue('item', 'unitcostoverride', itemIndex);
          nlapiLogExecution('DEBUG', 'setPurchasePriceBeforeSubmit', 'itemPOPriceSetOnUI::' + itemPOPriceSetOnUI);
          var itemPOPrice = null;
          if (itemId)
            itemPOPrice = nlapiLookupField('item', itemId, 'cost');
          if (itemPOPriceSetOnUI) {
            //nlapiLogExecution('DEBUG','setPurchasePriceBeforeSubmit','In if::');
            nlapiSetLineItemValue('item', 'unitcostoverride', itemIndex, parseFloat(itemPOPriceSetOnUI));
            return true;
          } else if (itemPOPrice) {
            //nlapiLogExecution('DEBUG','setPurchasePriceBeforeSubmit','In if::');
            nlapiSetLineItemValue('item', 'unitcostoverride', itemIndex, parseFloat(itemPOPrice));
          } else {
            //nlapiLogExecution('DEBUG','setPurchasePriceBeforeSubmit','In else::');
            var erroObj = nlapiCreateError('User Error', 'Please enter a value in "Override Rate" column for Item '+nlapiGetLineItemText('item', 'item', itemIndex)+' since there is no Purchase Price maintained at the Item Master level');
            throw erroObj;
          }
        }
      }
    }
  }
}

//Function to search Transaction Type
function searchRecordType(createdFrom) {
  try {
    var recordFilters = [],
      recordColumns = [];
    recordFilters.push(new nlobjSearchFilter('internalid', null, 'is', createdFrom), (new nlobjSearchFilter('mainline', null, 'is', 'T')));
    recordColumns.push(new nlobjSearchColumn('recordtype'));
    var searchRMARecord = nlapiSearchRecord('transaction', null, recordFilters, recordColumns);
    if (searchRMARecord) {
      var recordType = searchRMARecord[0].getValue('recordtype');
      return recordType;
    }
  } catch (err) {
    nlapiLogExecution('ERROR', 'Error in searchRecordType::', err);
  }
}
