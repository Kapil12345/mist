/*******************************************************************
 *
 *
 * Name: Mist_UE_create_serial_numbers.js
 * Script Type: User Event
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used to populate custom fields of inventory number on inventory count.
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */
function updateSerializedInventory(type, form) {
  try {
    var recordType = nlapiGetRecordType(),
      recStatus;
    //var recStatus = nlapiGetFieldValue('orderstatus');
    if (type == 'view') {
      var fileId;
      //var recObj = nlapiLoadRecord(recordType, nlapiGetRecordId());
      var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_mist_ss_upload_serial_nos', 'customdeploy_mist_ss_upload_serial_nos');
      suiteletURL += '&custparam_record_type=' + recordType + '&custparam_record_id=' + nlapiGetRecordId();
      nlapiLogExecution('DEBUG', 'File Field, Status', nlapiGetFieldValue('custbody_record_file_id') + '$$$' + nlapiGetFieldValue('custbody_record_status_updated'));
      fileId = nlapiGetFieldValue('custbody_record_file_id');
      var invDetailsStatus = nlapiGetFieldValue('custbody_record_status_updated');
      if (recordType == 'purchaseorder') { // && fileId
        recStatus = nlapiGetFieldValue('orderstatus');
        if (recStatus != 'F' && recStatus != 'G' && recStatus != 'H') { //Pending Billing, Fully Billed and Closed PO don't show up the button
          var buttonOnPO = form.addButton('custpage_btnserialno', 'Upload Serial Numbers', "window.open('" + suiteletURL + "','_blank','height=1000,width=1000');");
        }
      } else if (recordType == 'inventorycount') {
        var invCountRecStatus = nlapiGetFieldValue('statuskey');
        if (invCountRecStatus == 'B') { //Started status inventory count will show up the button
          var buttonOnInvRec = form.addButton('custpage_btnserialno', 'Upload Serial Numbers', "window.open('" + suiteletURL + "','_blank','height=1000,width=1000');");
        }
      }
      if (recordType == 'salesorder') { // && fileId
        recStatus = nlapiGetFieldValue('orderstatus');
        if (recStatus != 'A' && recStatus != 'C' && recStatus != 'F' && recStatus != 'G' && recStatus != 'H') { //Pending Approval,Cancelled,Pending Billing,Billed and Closed SO don't show up the button
          var buttonOnSO = form.addButton('custpage_btnserialno', 'Upload Serial Numbers', "window.open('" + suiteletURL + "','_blank','height=1000,width=1000');");
        }
      }
      if (recordType == 'transferorder') { //&& fileId
        recStatus = nlapiGetFieldValue('orderstatus');
        if (recStatus != 'A' && recStatus != 'C' && recStatus != 'H') { //Pending Approval, Rejected and Closed TO don't show up the button
          var buttonOnTO = form.addButton('custpage_btnserialno', 'Upload Serial Numbers', "window.open('" + suiteletURL + "','_blank','height=1000,width=1000');");
        }
      }
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error caught in before Load::', e);
  }
}
