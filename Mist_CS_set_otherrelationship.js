/*******************************************************************
 *
 *
 * Name: Mist_CS_set_otherrelationship.js
 * Script Type: Client Script
 * Version: 1.0
 *
 *
 * Author: Author Name
 * Purpose: A description of the purpose of this script
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function clientPageInit(type) {
  if (type == 'edit') //filter operation type to edit
  /*{
    alert('In Edit');
    var id = nlapiGetRecordId(); //get record id
    try //initiate try-catch
    {
      alert('In try');
      nlapiLoadRecord('customer', id); //check if record exist
    }
    catch (err)
    {
      alert('In catch');
      //nlapiRequestURL('https://system.na1.netsuite.com/app/common/entity/company.nl?id=' + id + '&totype=custjob&fromtype=vendor');
var response = nlapiRequestURL('https://debugger.na1.netsuite.com/app/common/entity/company.nl?id='+923+'&totype=custjob&fromtype=partner');
       //create lead
    //  var x = nlapiResolveURL('RECORD', 'vendor', id); //get back to vendor record
      //window.location = 'https://system.na1.netsuite.com' + x;
    }
  }*/
{
    var recid = nlapiGetRecordId();

	//only act if we get a record ID
    if (!recid) return;

	//ask the user if they really want to do this
	//if (!confirm("Are you sure you want to convert this partner to a customer?"))
  //return;

    // hitting this url will add the entity as a vendor
    var url ='https://system.netsuite.com/app/common/entity/company.nl?e=T&fromtype=partner&target=s_relation:otherrelationships&label=Other+Relationships&id='+recid+'&totype=custjob';
    nlapiRequestURL(url);

    // now try to load the customer record to confirm we did the work
    var worked = false;
    try{
        var customerRec = nlapiLoadRecord('customer', recid);
        customerRec.setFieldValue('entitystatus',13);
        nlapiSubmitRecord(customerRec,true,false);
        worked = true;
    }catch(e){
        worked = false;
    }

	//if it worked, redirect the user to the entity vendor view
    if (worked){
      //alert('worked=='+worked);
        var url = nlapiResolveURL('RECORD', 'customer', recid);
        document.location = url;
        return;
    }
}
}
