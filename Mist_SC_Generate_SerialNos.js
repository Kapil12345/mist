/*******************************************************************
 *
 *
 * Name: Mist_SC_Generate_SerialNos.js
 * Script Type: Scheduled
 * @version 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used to upload the serial numbers in the system based on the CSV file provided
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

//Function to parse CSV file
function parseCSVFile(type) {
  var recordObj;
  var context, recType, recId, fileId;
  try {
    nlapiLogExecution('DEBUG', 'parseCSVFile', '****SC Script Called****');
    context = nlapiGetContext();
    //nlapiLogExecution('DEBUG', 'parseCSVFile', 'context USER::' + context.getUser());
    //  var countId = 8; //context.getSetting('SCRIPT', 'inventorycountid');
    fileId = context.getSetting('SCRIPT', 'custscript_fileid'); //2152; //context.getSetting('SCRIPT', 'fileid');
    recType = context.getSetting('SCRIPT', 'custscript_record_type');
    recId = context.getSetting('SCRIPT', 'custscript_record_id');
    nlapiLogExecution('DEBUG', 'parseCSVFile', 'fileId::' + fileId + 'recType::' + recType + 'recId::' + recId);
    var file = nlapiLoadFile(fileId).getValue();
    var log = "";
    var data = "";

    var results = Papa.parse(file, {
      header: true
    });
    recordObj = nlapiLoadRecord(recType, recId);
    if (recordObj)
      generateSerialNumbers(results, fileId, recordObj, recType, recId, context);
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error in parseCSVFile::', e);
    nlapiLoadRecord(recType, recId);
    recordObj.setFieldValue('custbody_error_details', e);
    recordObj.setFieldValue('custbody_record_status_updated', '3'); //Failed
    nlapiSubmitRecord(recordObj, true);
  }
}

function generateSerialNumbers(results, fileId, recordObj, recType, recId, context) {
  try {
    var content = results.data; //Array of rows returned from the CSV file
    //nlapiLogExecution('DEBUG', 'generateSerialNumbers', 'results::' + JSON.stringify(results));
    //nlapiLogExecution('DEBUG', 'generateSerialNumbers', 'content::' + JSON.stringify(content));
    if (fileId) {
      var groupedItems = _.groupBy(content, 'Item'); //JSON object for all serial numbers and other fields with items will be returned

      recordObj.setFieldValue('custbody_record_file_id', fileId);
      recordObj.setFieldValue('custbody_record_status_updated', '1'); //Uploaded
      var invLineCount = recordObj.getLineItemCount('item');

      for (var itemIndex = 1; invLineCount && itemIndex <= invLineCount; itemIndex++) { //Loop for Item Count on record
        recordObj.selectLineItem('item', itemIndex);
        var itemName = recordObj.getCurrentLineItemText('item', 'item');
        var itemId = recordObj.getCurrentLineItemValue('item', 'item');
        var arrSerialNos = groupedItems[itemName], //Array of Serial Numbers and other fields with item of a single Line
          lineItemQty;

        if (arrSerialNos && arrSerialNos.length > 0) {
          //nlapiLogExecution('DEBUG', 'generateSerialNumbers::', 'arrSerialNos,arrSerialNos-->' + arrSerialNos + arrSerialNos.length);
          var serialNoSameItemArr = _.map(arrSerialNos, 'Serial_No'); //Array of Serial Numbers for single item will be returned

          var invDetailsObj;
          if (recType == 'inventorycount') {
            recordObj.setCurrentLineItemValue('item', 'countquantity', parseInt(serialNoSameItemArr.length));
            var viewSubRecord = recordObj.viewCurrentLineItemSubrecord('item', 'countdetail');
            if (viewSubRecord){
              var subrecordObj = recordObj.removeCurrentLineItemSubrecord('item', 'countdetail');
              recordObj.commitLineItem('item');
            }
              invDetailsObj = recordObj.createCurrentLineItemSubrecord('item', 'countdetail');
          } else if (recType == 'purchaseorder' || recType == 'transferorder' || recType == 'salesorder') {
            //recordObj.setCurrentLineItemValue('item', 'quantity', parseInt(serialNoSameItemArr.length));
            lineItemQty = recordObj.getCurrentLineItemValue('item', 'quantity');
            var viewSubRecord = recordObj.viewCurrentLineItemSubrecord('item', 'inventorydetail');
            if (viewSubRecord){
              var subrecordObj = recordObj.removeCurrentLineItemSubrecord('item', 'inventorydetail');
              recordObj.commitLineItem('item');
            }
              invDetailsObj = recordObj.createCurrentLineItemSubrecord('item', 'inventorydetail');
          }

          if (recType == 'inventorycount') { //For Inventory Count
            for (var serialNoIndex = 0; serialNoIndex <= serialNoSameItemArr.length; serialNoIndex++) {
              var getSingleSerialNo = serialNoSameItemArr[serialNoIndex];
              if (invDetailsObj) {
                invDetailsObj.selectNewLineItem('inventorydetail');
                if (getSingleSerialNo) {
                  nlapiLogExecution('DEBUG', 'generateSerialNumbers', getSingleSerialNo);
                  invDetailsObj.setCurrentLineItemValue('inventorydetail', 'inventorynumber', getSingleSerialNo);
                  invDetailsObj.setCurrentLineItemValue('inventorydetail', 'inventorystatus', '1');
                  invDetailsObj.commitLineItem('inventorydetail');
                }
              }
            }
          } else if (recType == 'purchaseorder' || recType == 'transferorder' || recType == 'salesorder') {
            if (serialNoSameItemArr.length <= lineItemQty) { //For Purchase order when Serial Nos on File are less than line quantity of PO
              for (var snoIndex = 0; snoIndex < serialNoSameItemArr.length; snoIndex++) {
                var getSingleSerialNoPO = serialNoSameItemArr[snoIndex];
                if (invDetailsObj) {
                  invDetailsObj.selectNewLineItem('inventoryassignment');
                  if (getSingleSerialNoPO) {
                    if(recType == 'purchaseorder')
                      invDetailsObj.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', getSingleSerialNoPO);
                    else if(recType == 'transferorder' || recType == 'salesorder'){
                      var serialNoId = getSerialNumbers(getSingleSerialNoPO, itemId, recordObj);
                      invDetailsObj.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', serialNoId);
                    }
                    invDetailsObj.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', '1');
                    invDetailsObj.commitLineItem('inventoryassignment');
                  }
                }
              }
            } else if (serialNoSameItemArr.length > lineItemQty) { //For Purchase order when Serial Nos on File are more than line quantity of PO
              for (var snoIndex = 0; snoIndex < lineItemQty; snoIndex++) {
                var getSingleSerialNoPO = serialNoSameItemArr[snoIndex];
                if (invDetailsObj) {
                  invDetailsObj.selectNewLineItem('inventoryassignment');
                  if (getSingleSerialNoPO) {
                    if(recType == 'purchaseorder')
                      invDetailsObj.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', getSingleSerialNoPO);
                      else if(recType == 'transferorder' || recType == 'salesorder'){
                        var serialNoId = getSerialNumbers(getSingleSerialNoPO, itemId, recordObj);
                        nlapiLogExecution('DEBUG', 'updateSerialNumbersFields length-->', 'serialNoId-->'+serialNoId);
                        invDetailsObj.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', serialNoId);
                      }
                    //invDetailsObj.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', getSingleSerialNoPO);
                    invDetailsObj.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', '1');
                    invDetailsObj.commitLineItem('inventoryassignment');
                  }
                }
              }
            }
          }
          if (invDetailsObj)
            invDetailsObj.commit();
        }
        recordObj.commitLineItem('item');
      }
      recordObj.setFieldValue('custbody_record_status_updated', '2'); //Serial Numbers Updated
      var recSubmitId = nlapiSubmitRecord(recordObj, true);
      if (recSubmitId) {
        //var emailSubject = 'Record has been Updated';
        //var emailBody = recType + ' record ' + 'with Id ' + recSubmitId + ' has been updtated';
        //nlapiSendEmail(context.getUser(), context.getUser(), emailSubject, emailBody);
        //Function to update the fields of serial numbers.
        updateSerialNumbersFields(content, groupedItems, recType, recSubmitId,context);
      }
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error in while submitting record', e);
    if (recId && recType) {
      var errorRecObj = nlapiLoadRecord(recType, recId);
      recordObj.setFieldValue('custbody_record_file_id', fileId);
      recordObj.setFieldValue('custbody_error_details', e);
      recordObj.setFieldValue('custbody_record_status_updated', '3'); //Failed
      nlapiSubmitRecord(recordObj, true);
    }
  }
}

function updateSerialNumbersFields(content, groupedItems, recType, recSubmitId,context) {
  try {
    if (recSubmitId && recType) {
      var transFilters = [],
        transColumns = [];
      transFilters.push(new nlobjSearchFilter("internalid", "transaction", "anyof", recSubmitId));
      transColumns.push(new nlobjSearchColumn("internalid", "item", "GROUP"), new nlobjSearchColumn("itemid", "item", "GROUP"), new nlobjSearchColumn("internalid", "inventoryNumber", "GROUP").setSort(false), new nlobjSearchColumn("inventorynumber", "inventoryNumber", "GROUP"));
      var inventoryDetailSearch = nlapiCreateSearch("inventorydetail", transFilters, transColumns);
      var searchResults = inventoryDetailSearch.runSearch();
      var length = getResultsLength(searchResults);
      nlapiLogExecution('DEBUG', 'updateSerialNumbersFields length-->', length);
      var resultIndex = 0,
        resultStep = 1000,
        resultSet, serialNoArr = [];
      do {
        resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
        resultIndex = parseInt(resultIndex + resultStep);
        for (var searchIndex = 0; searchIndex < resultSet.length; searchIndex++) {
          var serialNoObj = {};
          serialNoObj.itemId = resultSet[searchIndex].getValue("internalid", "item", "GROUP");
          serialNoObj.itemName = resultSet[searchIndex].getValue("itemid", "item", "GROUP");
          serialNoObj.serialNoId = resultSet[searchIndex].getValue("internalid", "inventoryNumber", "GROUP");
          serialNoObj.serialNo = resultSet[searchIndex].getValue("inventorynumber", "inventoryNumber", "GROUP");

          if (serialNoObj.serialNo && serialNoObj.itemId) {
            serialNoArr.push(serialNoObj);
          }
        }
      } while (resultSet.length > 0);

      var arrAllItems = Object.keys(groupedItems); //Keys of CSV grouped Item returned as item name
      for (var itemIndex = 0; itemIndex < arrAllItems.length; itemIndex++) // Loop on array of items from CSV
      {
        var arrSerialNosObj = groupedItems[arrAllItems[itemIndex]]; //Array of serialNos and fields data of single item from CSV
        for (var serIndex = 0; serIndex < arrSerialNosObj.length; serIndex++) //Loop on array of serialNos of single item from CSV
        {
          var currCSVRow = arrSerialNosObj[serIndex]; //Single current CSV row containing serial nos and other data
          var matchedSerialNo = _.find(serialNoArr, function(o) { // Matched Serial No Object from CSV compared from NS Search created above
            return o.itemName === currCSVRow.Item && o.serialNo === currCSVRow['Serial_No'];
          });

          if (matchedSerialNo) {
            var fieldsArr = ['custitemnumber_carton_id', 'custitemnumber_claim_code', 'custitemnumber_mac_id', 'custitemnumber_pallet_id', 'custitemnumber_invoice_no'],
              valuesArr = [currCSVRow.carton_Id, currCSVRow.claim_Code, currCSVRow.mac_Id, currCSVRow.pallet_Id, currCSVRow.invoice_No];
              //Check Governance Limit of script
            checkUsageGovernance();
            nlapiSubmitField('inventorynumber', matchedSerialNo.serialNoId, fieldsArr, valuesArr);
          }
        }
      }
      nlapiSubmitField(recType,recSubmitId,'custbody_record_status_updated','4');
      var emailSubject = 'Record has been Updated';
      var emailBody = recType + ' record ' + 'with Id ' + recSubmitId + ' has been updated';
      nlapiSendEmail(context.getUser(), context.getUser(), emailSubject, emailBody);
    }
  } catch (err) {
    nlapiLogExecution('ERROR', 'Error in updateSerialNumbersFields::', err);
  }
}

//Function to get the search result length
function getResultsLength(results) {
  var length = 0;
  var count = 0,
    pageSize = 100;
  var currentIndex = 0;
  do {
    count = results.getResults(currentIndex, currentIndex + pageSize).length;
    currentIndex += pageSize;
    length += count;
  }
  while (count == pageSize);
  return length;
}

//Function to get the governance limit of script
function checkUsageGovernance() {
  var objContext = nlapiGetContext();
  if (objContext.getRemainingUsage() <= 200) {
    var stateMain = nlapiYieldScript();
    if (stateMain.status == 'FAILURE') {
      nlapiLogExecution("DEBUG", "Failed to yield script (do-while), exiting: Reason = " + stateMain.reason + " / Size = " + stateMain.size);
      throw "Failed to yield script";
    } else if (stateMain.status == 'RESUME') {
      nlapiLogExecution("DEBUG", "Resuming script (do-while) because of " + stateMain.reason + ". Size = " + stateMain.size);
    }
  }
}

function getSerialNumbers(getSingleSerialNoPO, itemValue,recordObj){
  //nlapiLogExecution('DEBUG','itemValue',itemValue+'typeof'+typeof(itemValue));
  var location = recordObj.getFieldValue('location');
  var serialFilters = [], serialColumns = [];
  serialFilters.push(new nlobjSearchFilter('item', null, 'anyof',parseInt(itemValue)));
  serialFilters.push(new nlobjSearchFilter('inventorynumber',null, 'is',getSingleSerialNoPO));
  serialFilters.push(new nlobjSearchFilter('location',null, 'is',location));
  serialFilters.push(new nlobjSearchFilter('quantityonhand',null, 'greaterthan','0'));
  serialColumns.push(new nlobjSearchColumn('inventorynumber'));

  var serialNumbers = nlapiSearchRecord('inventorynumber',null,serialFilters,serialColumns);

  if(serialNumbers){
    nlapiLogExecution('DEBUG','serialNumber Id',serialNumbers[0].getId());
    return serialNumbers[0].getId();
  }
}
