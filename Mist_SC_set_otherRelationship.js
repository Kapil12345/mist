/*******************************************************************
 *
 *
 * Name: Mist_SC_set_otherRelationship.js
 * Script Type: Scheduled
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose:This script is used to set partner as customer in other relationships
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function setPartnerAsCustomer(type) {
var partnerIntIds = ['923','924','925','926','927','928','929','930','931','932','933','934','1223','935','936','937','938','939','940','941','942','943','944','945','946','947','948','949','950','951','952','953','954','1224','955','956','957','958','959','960','961','962','963','964','965','965','966','967','968','969','970','971','972','973','974','975','976','977','978','979','980','981','982','983','984','985','986','987','988','989','990','991','992','993','994','995','996','997','998','999','1000','1001','1002','1003','1004','1005','1225','1008','1009','1010','1011','1012','1013','1014','1015','1016','1017','1018','1019','1020','1021','1022','1023','1024','1025','1026','1027','1028','1029','1030','1031','1032','1033','1034','1035','1036','1037','1038','1039','1040','1226','1041','1042','1043','1044','1045','1046','1047','1048','1049','1050','1051','1052','1053','1227','1054','1055','1056','1057','1058','1059','1060','1061','1062','1063','1064','1065','1066','1067','1068','1069','1155','1070','1071','1072','1073','1074','1075','1076','1077','1228','1078','1079','1080','1081','1082','1083','1084','1085','1086','1087','1088','1089','1090','1091','1092','1093','1094','1095','1096','1097','1098','1099','1100','1101','1102','1103','1104','1105','1106','1107','1108','1109','1110','1111','1112','1113','1114','1115','1116','1117','1118','1119','1120','1229','1323','1121','1122','1123','1124','1125','1126','1127','1128','1129','1130','1131','1132','1133','1134','1135','1136','1137','1138','1139','1140','1141','1142','1143','1144','1145','1146','1231','1147','1148','1149','1127','1150','1151','1152','1078','1153','1154','1155','1079','1156','1157','1158'];
var partnerFilters = [], partnerColumns = [];
partnerFilters.push(new nlobjSearchFilter('internalid',null,'anyof',partnerIntIds));
partnerColumns.push(new nlobjSearchColumn('internalid'));
var searchPartners = nlapiSearchRecord('partner',null,partnerFilters,partnerColumns);
//for(var partnerIndex=0; searchPartners && partnerIndex<searchPartners.length; partnerIndex++)
for(var partnerIndex=0; partnerIndex<searchPartners.length; partnerIndex++){
  nlapiLogExecution('DEBUG','setPartnerAsCustomer','searchPartners length-->'+searchPartners.length);
  var recid = searchPartners[partnerIndex].getValue('internalid'); //nlapiGetRecordId();
  //only act if we get a record ID
  if (!recid) return;

  //ask the user if they really want to do this
  //if (!confirm("Are you sure you want to convert this partner to a customer?"))
    //return;

  // hitting this url will add the entity as a vendor
  var url = 'https://system.netsuite.com/app/common/entity/company.nl?e=T&fromtype=partner&target=s_relation:otherrelationships&label=Other+Relationships&id=' + recid + '&totype=custjob';
  nlapiRequestURL(url);

  // now try to load the customer record to confirm we did the work
  var worked = false;
  try {
    var customerRec = nlapiLoadRecord('customer', recid);
    customerRec.setFieldValue('entitystatus', 13);
    var customerId = nlapiSubmitRecord(customerRec, true, false);
    nlapiLogExecution('DEBUG','setPartnerAsCustomer','customerId-->'+customerId);
    worked = true;
  } catch (e) {
    worked = false;
  }

  //if it worked, redirect the user to the entity vendor view
  if (worked) {
    //alert('worked=='+worked);
    var url = nlapiResolveURL('RECORD', 'customer', recid);
    document.location = url;
    return;
  }
}
}
