/*******************************************************************
 *
 *
 * Name: Mist_UE_Rev_Rec_Customization.js
 * Script Type: User Event
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used on revenue recognition, revenue element and item fulfillment for their updation.
 * Script: customscript_mist_ue_rev_rec_customize
 * Deploy: customdeploy_mist_ue_rev_rec_customize
 *
 *
 * ******************************************************************* */
//After Submit event to set the Rev Rec On Hold
function updateRevenueRec(type) {
  try {
    if (type == 'create' || type == 'edit') {
      var createdFrom = nlapiGetFieldValue('createdfrom');
      var revPlanType = nlapiGetFieldValue('revenueplantype');
      var revRecHold = nlapiGetFieldValue('holdrevenuerecognition');
      if (type == 'edit') {
        var oldRevRecObj = nlapiGetOldRecord();
        var oldrevRecHold = oldRevRecObj.getFieldValue('holdrevenuerecognition');
        nlapiLogExecution('DEBUG', 'updateRevenueRec', 'oldrevRecHold,revRecHold::' + oldrevRecHold+'&&&'+revRecHold);
        if (createdFrom && revPlanType == 'ACTUAL' && oldrevRecHold !== 'T') {
          var getItemFulfillmentInfo = searchRevenueElement(createdFrom,nlapiGetRecordId());
      	}
      } else if (type == 'create' && createdFrom && revPlanType == 'ACTUAL') {
        var getItemFulfillmentInfo = searchRevenueElement(createdFrom,nlapiGetRecordId());
      }
    }
  } catch (err) {
    nlapiLogExecution('ERROR','Error in updateRevenueRec::',err);
  }
}

//Function to search Customer on Revenue Element
function searchRevenueElement(createdFrom,revenuePlanId) {
  try {
    if (createdFrom) {
      var revElementFilters = [],
        revElementColumns = [];
      revElementFilters.push(new nlobjSearchFilter('internalid', null, 'is', createdFrom));
      revElementColumns.push(new nlobjSearchColumn('entity'));
      var searchRevElementRec = nlapiSearchRecord('revenueelement', null, revElementFilters, revElementColumns);
      if (searchRevElementRec) {
        var revElementCustomer = searchRevElementRec[0].getValue('entity');
        if (revElementCustomer) {
          var searchPODCustomer = getPODCustomer(revElementCustomer);
          if(searchPODCustomer){
            nlapiSubmitField('revenueplan',revenuePlanId,'holdrevenuerecognition','T');
          }
        }
      }
    }
  } catch (err) {
    nlapiLogExecution('ERROR','Error in searchRevenueElement::',err);
  }
}

//Function to search Customer on ccustom record "POD Customers" matched with customer on Revenue Element
function getPODCustomer(revElementCustomer) {
  try {
    nlapiLogExecution('DEBUG', 'updateRevenueRec', 'revElementCustomer::' + revElementCustomer);
    if (revElementCustomer) {
      var PODFilters = [], PODColumns = [];
      PODFilters.push(new nlobjSearchFilter('custrecord_pod_customers',null,'is',revElementCustomer),new nlobjSearchFilter('isinactive',null,'is','F'));
      var searchPODCustomer = nlapiSearchRecord('customrecord_pod_customers',null,PODFilters,null);
      return searchPODCustomer;
    }
  } catch (err) {
nlapiLogExecution('ERROR','Error in getPODCustomer::',err);
  }
}
